<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class SlackController extends Controller
{

    public function login()
    {
        return Socialite::driver('slack')->redirect();
    }

    public function redirectToProvider()
    {
        return 123;
    }
}
