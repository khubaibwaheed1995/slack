<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SlackController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $clientId = config('services.slack')['client_id'];
    return view('welcome')->with('clientId',$clientId);
});



Route::get('login', [SlackController::class,'login']);
Route::get('path', [SlackController::class,'redirectToProvider']);